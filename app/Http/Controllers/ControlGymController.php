<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;



class ControlGymController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'dni' => 'required|int|min:8',
            'sex' => 'required|string|max:50',
            'plan' => 'required|string|max:150',
            'payment' => 'required|boolean',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()) {
                return array($validator->errors());
            }
        }

        $userCount = User::all()->count();

        $user = User::create([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'dni' => $request->dni,
            'sex' => $request->sex,
            'plan' => $request->plan,
            'rol' => $userCount > 0 ? 1 : 0,
            'payment' => $request->payment,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json([
            'status' => true,
            'data' => $user,
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'dni' => 'required|int|min:8',
            'sex' => 'required|string|max:50',
            'plan' => 'required|string|max:150',
            'payment' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()) {
                return array($validator->errors());
            }
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->dni = $request->dni;
        $user->sex = $request->sex;
        $user->plan = $request->plan;
        $user->payment = $request->payment;
        $user->save();

        return response()->json([
            'status' => true,
            'message' => 'Actualizado correctamente',
            'data' => $user,
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $user = User::find($id);
        $userCount = User::all()->count();
        if ($user) {
            if ($user->rol === 0 && $userCount < 2) {
                $m = 'No se puede eliminar el administrador';
            } else {
                $user->delete();
                $m = 'Se elimino correectamente';
            }
        } else {
            $m = 'No existe el registro';
        }
        return response()->json([
            'status' => true,
            'message' => $m,
        ], 201);
    }
}
